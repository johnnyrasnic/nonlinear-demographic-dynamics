import matplotlib.pyplot as plt
import diffEq
import beetlePlot
import bifurTable

plot = beetlePlot.beetlePlot
bifurPlot = beetlePlot.bifurPlot
popTable = diffEq.diffEq
bifurTable = bifurTable.bifurTable

# coeff matrix = [b, mu_a, mu_l, c_ea, c_el, c_pa]
coeff = [20, 0.6, 0.51, 0.01, 0, 0.09]

# initial population = [L_0, P_0, A_0]
LPA = [75,75,75]

# poptable generates a population table given an LPA and coeff matrix with a
# given time step, plot will take any poptable as an argument and generate
# a plot

#plot(popTable(LPA,coeff,100))

# bifurTable will generate a table of values for the input population LPA,
# coeff matrix, and population figure (in this case "larvae")

#bifurTable(LPA,coeff,"larvae")

# bifurTable takes the same arguments as bifurTable does, but will generate a
# plot instead

#bifurPlot(LPA,coeff,"larvae")
