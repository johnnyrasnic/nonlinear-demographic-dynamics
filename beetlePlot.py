import matplotlib.pyplot as plt
import numpy
import bifurTable

bifurTable = bifurTable.bifurTable

def beetlePlot(popTable):
    plt.figure()
    g1 = plt.subplot(311)
    plt.plot(popTable["timestep"],popTable["larvae"], 'g.', markersize=0.5)
    plt.xlabel('T = generations')
    plt.ylabel('L = Larvae Population')

    g2 = plt.subplot(312, sharey=g1)
    plt.plot(popTable["timestep"], popTable["pupae"], 'b-o')
    plt.xlabel('T = time series')
    plt.ylabel('P = Pupae Population')

    g3 = plt.subplot(313, sharey=g1)
    plt.plot(popTable["timestep"], popTable["adults"], 'm-o')
    plt.xlabel('T = Timestep')
    plt.ylabel('A = Adults')

    plt.show()

def bifurPlot(LPA, coeff,stageStr):
    mu_a = numpy.linspace(0,1,100)

    plt.figure()
    g1 = plt.subplot(311)
    plt.plot(mu_a, bifurTable(LPA,coeff,stageStr), 'g.', markersize=0.5)
    plt.xlabel("mu_a")
    plt.ylabel("number of " + stageStr)
    plt.show()
