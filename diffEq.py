import math
import numpy

def diffEq(LPA, coeff, t):
    T = numpy.linspace(0,t,t+1)
    L = [LPA[0]]
    P = [LPA[1]]
    A = [LPA[2]]

    b = coeff[0]
    mu_a = coeff[1]
    mu_l = coeff[2]
    c_ea = coeff[3]
    c_el = coeff[4]
    c_pa = coeff[5]


    i = 0
    
    while i <= t-1: 
        L.append(b*A[i]*math.exp(-1*c_ea*A[i] - c_el*L[i]))
        P.append(L[i]*(1-mu_l))
        A.append(P[i]*math.exp(-1*c_pa*A[i])+A[i]*(1-mu_a))
        i += 1 

    table = {
            "timestep": T,
            "larvae": L,
            "pupae": P,
            "adults": A
            }

    return table

