import diffEq
import numpy

popTable = diffEq.diffEq

# note: mu_a in the coefficient matrix will not be used
def bifurTable(LPA, coeff, stageStr):
    mu_a = numpy.linspace(0,1,100)
    equil = []
    for i in range(0,100):
        coeff[1] = mu_a[i]
        arr = popTable(LPA,coeff,1000)
        sarr = arr[stageStr]
        equil.append(sarr[899:999])

    return equil



