# Info 
This is Python code I wrote for an independent study on the paper [Nonlinear Demographic Dynamics: Mathematical Models, Statistical Methods, and Biological Experiments
written by Brian Dennis, Robert A. Desharnais, J. M. Cushing and R. F. Costantino](https://www.jstor.org/stable/2937060). This code can  run the time series population equations and generate a table and plot, as well as generate bifurcation diagrams with the mu\_a parameter. This code is not organized in any particular way, and I did not know any Python prior to coding this up, so the code may be rather messy/redundant.

# Dependencies
* numpy 1.17
* matplotlib 3.1

# Installation and Instructions
Make sure you have git installed. Run `git clone https://gitlab.com/johnnyrasnic/nonlinear-demographic-dynamics.git` to clone the repository. Edit the file `beetles.py` and change the parameters going into the functions, as well as uncommenting desired function. After editing, simply run `python3 beetles.py` with command line in the directory with the code.

# License
This code is licensed under the GPLv3 copyleft license. A copy of this license can be found under LICENSE
